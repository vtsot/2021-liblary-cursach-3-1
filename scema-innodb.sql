

CREATE DATABASE IF NOT EXISTS `2021-liblary-cursach-3-1-innodb` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `2021-liblary-cursach-3-1-innodb`;

DROP TABLE IF EXISTS `authors`;
CREATE TABLE IF NOT EXISTS `authors` (
    `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    `first_name` varchar(255) DEFAULT NULL,
    `last_name` varchar(255) DEFAULT NULL,
    PRIMARY KEY (`id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `authors_books`;
CREATE TABLE IF NOT EXISTS `authors_books` (
    `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    `author_id` int(10) UNSIGNED DEFAULT NULL,
    `book_id` int(10) UNSIGNED DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `fk_author_id` (`author_id`),
    KEY `fk_book_id` (`book_id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `books`;
CREATE TABLE IF NOT EXISTS `books` (
    `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    `title` varchar(255) DEFAULT NULL,
    `description` longtext,
    `quantity` int(11) NOT NULL DEFAULT '0',
    PRIMARY KEY (`id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `orders`;
CREATE TABLE IF NOT EXISTS `orders` (
    `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    `book_id` int(10) UNSIGNED DEFAULT NULL,
    `user_id` int(10) UNSIGNED DEFAULT NULL,
    `reading_type` int(10) UNSIGNED NOT NULL,
    `status` smallint(5) UNSIGNED DEFAULT NULL,
    `start_at` date DEFAULT NULL,
    `end_at` date DEFAULT NULL,
    `quantity` int(11) NOT NULL DEFAULT '0',
    `created_at` datetime DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `fk_book_id` (`book_id`),
    KEY `fk_user_id` (`user_id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `reading`;
CREATE TABLE IF NOT EXISTS `reading` (
    `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    `book_id` int(10) UNSIGNED DEFAULT NULL,
    `user_id` int(10) UNSIGNED DEFAULT NULL,
    `reading_type` int(10) UNSIGNED NOT NULL,
    `quantity` int(11) NOT NULL DEFAULT '0',
    `start_at` date DEFAULT NULL,
    `end_at` date DEFAULT NULL,
    `prolong_at` date DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `fk_book_id` (`book_id`),
    KEY `fk_user_id` (`user_id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
    `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    `username` varchar(60) NOT NULL,
    `password` varchar(64) NOT NULL,
    `email` varchar(60) DEFAULT NULL,
    `salt` varchar(255) NOT NULL,
    `roles` json NOT NULL,
    `first_name` varchar(255) DEFAULT NULL,
    `last_name` varchar(255) DEFAULT NULL,
    `active` tinyint(1) NOT NULL DEFAULT '1',
    PRIMARY KEY (`id`),
    KEY `idx_active` (`active`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE `authors_books`
    ADD CONSTRAINT `fk_authors_books_book_id`   FOREIGN KEY (`book_id`)   REFERENCES `books`   (`id`) ON DELETE CASCADE,
    ADD CONSTRAINT `fk_authors_books_author_id` FOREIGN KEY (`author_id`) REFERENCES `authors` (`id`) ON DELETE CASCADE;

ALTER TABLE `orders`
    ADD CONSTRAINT `fk_orders_book_id` FOREIGN KEY (`book_id`) REFERENCES `books` (`id`),
    ADD CONSTRAINT `fk_orders_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

ALTER TABLE `reading`
    ADD CONSTRAINT `fk_reading_book_id` FOREIGN KEY (`book_id`) REFERENCES `books` (`id`) ON DELETE CASCADE,
    ADD CONSTRAINT `fk_reading_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
