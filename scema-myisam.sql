
CREATE DATABASE IF NOT EXISTS `2021-liblary-cursach-3-1-myisam` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `2021-liblary-cursach-3-1-myisam`;

DROP TABLE IF EXISTS `authors`;
CREATE TABLE IF NOT EXISTS `authors` (
    `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    `first_name` varchar(255) DEFAULT NULL,
    `last_name` varchar(255) DEFAULT NULL,
    PRIMARY KEY (`id`)
    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TRIGGER IF EXISTS `author_delete_authors`;
CREATE TRIGGER `author_delete_authors` AFTER DELETE ON `authors` FOR EACH ROW DELETE FROM `authors_books` WHERE author_id = OLD.id;

DROP TABLE IF EXISTS `authors_books`;
CREATE TABLE IF NOT EXISTS `authors_books` (
    `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    `author_id` int(10) UNSIGNED DEFAULT NULL,
    `book_id` int(10) UNSIGNED DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `fk_author_id` (`author_id`),
    KEY `fk_book_id` (`book_id`)
    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `books`;
CREATE TABLE IF NOT EXISTS `books` (
    `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    `title` varchar(255) DEFAULT NULL,
    `description` longtext,
    `quantity` int(11) NOT NULL DEFAULT '0',
    PRIMARY KEY (`id`)
    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TRIGGER IF EXISTS `book_delete_authors`;
CREATE TRIGGER `book_delete_authors` AFTER DELETE ON `books` FOR EACH ROW DELETE FROM `authors_books` WHERE book_id = OLD.id;

DROP TRIGGER IF EXISTS `book_delete_orders`;
CREATE TRIGGER `book_delete_orders` AFTER DELETE ON `books` FOR EACH ROW DELETE FROM `orders` WHERE book_id = OLD.id;

DROP TRIGGER IF EXISTS `book_delete_readings`;
CREATE TRIGGER `book_delete_readings` AFTER DELETE ON `books` FOR EACH ROW DELETE FROM `reading` WHERE book_id = OLD.id;

DROP TABLE IF EXISTS `orders`;
CREATE TABLE IF NOT EXISTS `orders` (
    `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    `book_id` int(10) UNSIGNED DEFAULT NULL,
    `user_id` int(10) UNSIGNED DEFAULT NULL,
    `reading_type` int(10) UNSIGNED NOT NULL,
    `status` smallint(5) UNSIGNED DEFAULT NULL,
    `start_at` date DEFAULT NULL,
    `end_at` date DEFAULT NULL,
    `quantity` int(11) NOT NULL DEFAULT '0',
    `created_at` datetime DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `fk_book_id` (`book_id`),
    KEY `fk_user_id` (`user_id`)
    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `reading`;
CREATE TABLE IF NOT EXISTS `reading` (
    `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    `book_id` int(10) UNSIGNED DEFAULT NULL,
    `user_id` int(10) UNSIGNED DEFAULT NULL,
    `reading_type` int(10) UNSIGNED NOT NULL,
    `quantity` int(11) NOT NULL DEFAULT '0',
    `start_at` date DEFAULT NULL,
    `end_at` date DEFAULT NULL,
    `prolong_at` date DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `fk_book_id` (`book_id`),
    KEY `fk_user_id` (`user_id`)
    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
    `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    `username` varchar(60) NOT NULL,
    `password` varchar(64) NOT NULL,
    `email` varchar(60) DEFAULT NULL,
    `salt` varchar(255) NOT NULL,
    `roles` json NOT NULL,
    `first_name` varchar(255) DEFAULT NULL,
    `last_name` varchar(255) DEFAULT NULL,
    `active` tinyint(1) NOT NULL DEFAULT '1',
    PRIMARY KEY (`id`),
    KEY `idx_active` (`active`)
    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TRIGGER IF EXISTS `user_delete_orders`;
CREATE TRIGGER `user_delete_orders` AFTER DELETE ON `users` FOR EACH ROW DELETE FROM `orders` WHERE user_id = OLD.id;

DROP TRIGGER IF EXISTS `user_delete_readings`;
CREATE TRIGGER `user_delete_readings` AFTER DELETE ON `users` FOR EACH ROW DELETE FROM `reading` WHERE `user_id` = OLD.id;
